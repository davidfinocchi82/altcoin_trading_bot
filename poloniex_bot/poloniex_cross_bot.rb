#millions of dollars 
#!/usr/bin/ruby

$LOAD_PATH << '.'
require 'byebug'
require 'lib/poloniex.rb'
require 'json'
require 'indicator/mixin'

pair_array = ["BTC_ETH", "BTC_XMR", "BTC_FCT", "BTC_LTC", "BTC_MAID", "BTC_XRP", "BTC_DASH", "BTC_STR", "BTC_DOGE", "BTC_BTS", "BTC_CLAM"]
puts "0: BTC_ETH\n1: BTC_XMR\n2: BTC_FCT\n3: BTC_LTC\n4: BTC_MAID\n5: BTC_XRP\n6: BTC_DASH\n7: BTC_STR\n8: BTC_DOGE\n9:BTC_BTS\n10: BTC_CLAM"
puts "Enter currency pair number 0-10: "
pair_index = Integer(gets) rescue nil

until pair_index.is_a?(Fixnum) do
  print "Please enter a number 0-10: "
  pair_index = Integer(gets) rescue nil
end
until pair_index < 10 do 
	puts "Enter a number 0-10: "
	pair_index = gets
end

poloniex_pair = pair_array[pair_index]
poloniex_base_currency = poloniex_pair.split("_")[0]
candles = []
candles_high = []
candles_low = []
candles_close = []
ema_10_arr = []
ema_21_arr = []
win_percent = 0.01
lose_percent = 0.01
candle_time_period = 300
candle_length = candle_time_period * 60
decimal_threshold_1 = 8

case poloniex_pair
when pair_array[0]
	api_key = 'UZAOA96A-JORQDI49-61V5YVVJ-S9WN85HT'
	api_secret = '04116b019c7ba064c8e55e18a6b4ee9ff9c1794840ec9ca19c84f1ac308767b5286f12b416d0daf224caea3ba3856424e6f49739ce9c233b6e737e6b34907709'
when pair_array[1]
	api_key = 'F4ON5NA8-6EGZWE14-HRLX9SEK-VPX31WY7'
	api_secret = '910b45dffec71066846947053614bb76cd20d11e233b9bf17428b893418b8c221da19e8a43ad1c6e3fa7fb11229f0d0ee414719d9b4c3e78a4a5f9cea02c8087'
when pair_array[2]
	api_key = 'VOS52MP7-KFX2JJ70-0J3BO0AK-UCDAQ1VC'
	api_secret = '1e4997779546340e70a399325d73c6067eaf3b10b88a29138a50dd5d60456e8ef1811efc7bb84caa533dd1b26a1d83de74958a7a56cad5a13e49b56aefcc560b'
when pair_array[3]
	api_key = '21E8CRNW-6J9EM6ZR-MGVVG2WM-RC2PPQ8I'
	api_secret = 'b787d9ce66b5196defec088ea77812658cbdaa695d9eaf2dd21be6fd8018ed82916cd55e7ea1cffffb5891ac0fdeed764acba2f80094d86414ab387ac58d1a74'
else
	puts "No API Key"
end

Poloniex.setup do | config |
    config.key = api_key
    config.secret = api_secret
end

def get_poloniex_balance(currency, nonce_count)
    poloniex_balance_result = Poloniex.balances(nonce_count)
    result = JSON.parse(poloniex_balance_result.body)["margin"][currency] rescue nil
    if result == nil
        return 0.0
    else
        return JSON.parse(poloniex_balance_result.body)["margin"][currency]
    end
end

def get_poloniex_bid(ticker, poloniex_pair)
    return JSON.parse(ticker.body)[poloniex_pair]["highestBid"]
end

def get_poloniex_ask(ticker, poloniex_pair)
    return JSON.parse(ticker.body)[poloniex_pair]["lowestAsk"]
end

def chart_data(poloniex_pair, end_time, candle_time_period, candle_length)
	data = JSON.parse(Poloniex.return_chart_data(poloniex_pair, candle_time_period, (Time.now.to_i - candle_length).round(7), end_time).body)
	return data
end

eth_btc_history = Poloniex.get_trade_history(poloniex_pair)
current_rate = JSON.parse(eth_btc_history.body).first["rate"].to_f
short = false
long = false
in_long_trade = false
in_short_trade = false
long_cross = false
short_cross = false
count = 0
nonce_count = 1
trade_price = 0.0
losing_trades = 0.0
winning_trades = 0.0
nonce_count = 1
end_time = 9999999999
init_balance = get_poloniex_balance(poloniex_base_currency, nonce_count =+ 1).to_f
poloniex_balance = init_balance
puts init_balance

File.delete("#{poloniex_pair}_log.txt") if File.exist?("#{poloniex_pair}_log.txt")
File.new("#{poloniex_pair}_log.txt", "w")

while true 
	last = Time.now
	begin
		time = Time.now.utc
	    	begin
			eth_btc_history = Poloniex.get_trade_history(poloniex_pair)
			current_rate = JSON.parse(eth_btc_history.body).first["rate"].to_f
		rescue => e
			puts "#{time} Error happended with current rate: #{e}"
			open("#{poloniex_pair}_log.txt", 'a') { |f|
				f.puts "Error happended somewhere: #{e}"
			}
			retry
		end
		puts time
		puts poloniex_pair
		puts "Rate: #{current_rate}"
		if in_long_trade == true 
			poloniex_ticker = Poloniex.ticker
			poloniex_bid = get_poloniex_bid(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
			if poloniex_bid < (trade_price - (trade_price * lose_percent))
				Poloniex.cancel_margin_position(poloniex_pair, nonce_count += 1)
				losing_trades = losing_trades + 1
				open("#{poloniex_pair}_log.txt", 'a') { |f|
					f.puts "#{time} Losing long trade"
					if winning_trades > 0 and losing_trades > 0
						f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
					end
					f.puts "Profit/Loss: #{"%f" % (poloniex_balance - init_balance)}"
				}
				poloniex_balance = get_poloniex_balance(poloniex_base_currency, nonce_count += 1).to_f
				short = false
				long = false
				in_long_trade = false
				in_short_trade = false
				long_cross = false
				short_cross = false
				trade_price = 0.0
			elsif poloniex_bid > (trade_price + (trade_price * win_percent))
				Poloniex.cancel_margin_position(poloniex_pair, nonce_count += 1)
				winning_trades = winning_trades + 1
				open("#{poloniex_pair}_log.txt", 'a') { |f|
					f.puts "#{time} Winning long trade"
					if winning_trades > 0 and losing_trades > 0
						f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
					end
					f.puts "Profit/Loss: #{"%f" % (poloniex_balance - init_balance)}"
				}
				poloniex_balance = get_poloniex_balance(poloniex_base_currency, nonce_count += 1).to_f
				short = false
				long = false
				in_long_trade = false
				in_short_trade = false
				long_cross = false
				short_cross = false
				trade_price = 0.0
			end
		end
		if in_short_trade == true
			poloniex_ticker = Poloniex.ticker
			poloniex_ask = get_poloniex_ask(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
	    		if poloniex_ask > (trade_price + (trade_price * lose_percent))
	    			Poloniex.cancel_margin_position(poloniex_pair, nonce_count += 1)
				losing_trades = losing_trades + 1
				open("#{poloniex_pair}_log.txt", 'a') { |f|
					f.puts "#{time} Losing short trade"
					if winning_trades > 0 and losing_trades > 0
						f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
					end
					f.puts "Profit/Loss: #{"%f" % (poloniex_balance - init_balance)}"
				}
				poloniex_balance = get_poloniex_balance(poloniex_base_currency, nonce_count += 1).to_f
				short = false
				long = false
				in_long_trade = false
				in_short_trade = false
				long_cross = false
				short_cross = false
				trade_price = 0.0
			elsif  poloniex_ask < (trade_price - (trade_price * win_percent))
				Poloniex.cancel_margin_position(poloniex_pair, nonce_count += 1)
				winning_trades = winning_trades + 1
				open("#{poloniex_pair}_log.txt", 'a') { |f|
					f.puts "#{time} Winning short trade"
					if winning_trades > 0 and losing_trades > 0
						f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
					end
					f.puts "Profit/Loss: #{"%f" % (poloniex_balance - init_balance)}"
				}
				poloniex_balance = get_poloniex_balance(poloniex_base_currency, nonce_count += 1).to_f
				short = false
				long = false
				in_long_trade = false
				in_short_trade = false
				long_cross = false
				short_cross = false
				trade_price = 0.0
			end
		end
		if time.min % 5 == 0 and time.sec == 0 and in_long_trade == false and in_short_trade == false
			bars = chart_data(poloniex_pair, end_time, candle_time_period, candle_length)
			bars = bars[0...-1]
			bars.reverse.each do |data|
				candles.push([data["high"], data["low"], data["close"]])
				candles_high.push(data["high"])
				candles_low.push(data["low"])
				candles_close.push(data["close"])
			end
			22.times do |x|
				x += x
				ema_10_arr.push(candles_close[x..-1].indicator(:ema, time_period: 10)[0])
				ema_21_arr.push(candles_close[x..-1].indicator(:ema, time_period: 21)[0])
			end
			ema_10 = ema_10_arr[0]
			ema_21 = ema_21_arr[0]
			puts "EMA 10: #{ema_10}"
			puts "EMA 21: #{ema_21}"
			if ema_10_arr[0] > ema_21_arr[0] and ema_10_arr[2] < ema_21_arr[2]
				long_cross = true 
				short_cross = false
			elsif ema_10_arr[0] < ema_21_arr[0] and ema_10_arr[2] > ema_21_arr[2]
				short_cross = true
				long_cross = false	
			end
			if ema_10 > (ema_21 + (ema_21* 0.0025)) and long_cross == true
				long = true
				short = false
			elsif ema_10 < (ema_21 - (ema_21 * 0.0025)) and short_cross == true
				long = false
				short = true
			end	
			if long == true and in_long_trade == false and in_short_trade == false
				#make long
				poloniex_ticker = Poloniex.ticker
				poloniex_ask = get_poloniex_ask(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
		    		amount = ((1 / poloniex_ask) * (poloniex_balance - (poloniex_balance * 0.80))).round(decimal_threshold_1)
				poloniex_order = Poloniex.buy(poloniex_pair, poloniex_ask, amount, nonce_count += 1)
				in_long_trade = true
				trade_price = poloniex_ask
				open("#{poloniex_pair}_log.txt", 'a') { |f|
					f.puts "#{time}, Making long trade"
					f.puts "#{poloniex_order}"
					# f.puts "Candles: #{candles[0]}"
					# f.puts "Candles close: #{candles_close[0]}\n"
				}
			elsif short == true and in_long_trade == false and in_short_trade == false
				#make short
				poloniex_ticker = Poloniex.ticker
				poloniex_bid = get_poloniex_bid(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
		    		amount = ((1 / poloniex_bid) * (poloniex_balance - (poloniex_balance * 0.80))).round(decimal_threshold_1)
		    		poloniex_order = Poloniex.sell(poloniex_pair, poloniex_bid, amount, nonce_count += 1)
				in_short_trade = true
				trade_price = poloniex_bid
				open("#{poloniex_pair}_log.txt", 'a') { |f|
					f.puts "#{time}, Making short trade"
					f.puts "#{poloniex_order}"
					# f.puts "Candles: #{candles[0]}"
					# f.puts "Candles close: #{candles_close[0]}\n"
				}
			end	
			count = count + 1
    		end
	rescue => e
	    puts "Error happended somewhere: #{e}"
	    open("#{poloniex_pair}_log.txt", 'a') { |f|
			f.puts "Error happended somewhere: #{e}"
		}
	    now = Time.now
	    _next = [last + 1,now].max
	    sleep (_next-now)
	    last = _next
	    retry
	end
	candles = []
	candles_high = []
	candles_low = []
	candles_close = []
	ema_10_arr = []
	macd_line = []
	signal = []
	nonce_count = 1
	now = Time.now
	_next = [last + 1,now].max
	sleep (_next-now)
	last = _next
end