require 'rest-client'
require 'openssl'
require 'addressable/uri'

module Poloniex

  class << self
    attr_accessor :configuration
  end

  def self.setup
    @configuration ||= Configuration.new
    yield( configuration )
  end

  class Configuration
    attr_accessor :key, :secret

    def intialize
      @key    = ''
      @secret = ''
    end
  end

  def self.ticker
    get 'returnTicker'
  end

  def self.volume
    get 'return24hVolume'
  end

  def self.order_book( currency_pair )
    get 'returnOrderBook', currencyPair: currency_pair
  end

  def self.get_trade_history( currency_pair )
    get 'returnTradeHistory', currencyPair: currency_pair
  end

  def self.balances( count )
    post 'returnAvailableAccountBalances', count, account: "all"
  end

  def self.open_orders( currency_pair, count )
    post 'returnOpenOrders', count, currencyPair: currency_pair
  end

  def self.post_trade_history( currency_pair, count )
    post 'returnTradeHistory', count, currencyPair: currency_pair
  end

  def self.create_order( type, currency_pair, rate, amount, count )
    post type, count, currencyPair: currency_pair, rate: rate, amount: amount
  end

  def self.buy( currency_pair, rate, amount, count )
    post 'marginBuy', count, currencyPair: currency_pair, rate: rate, amount: amount
  end

  def self.sell( currency_pair, rate, amount, count )
    post 'marginSell', count, currencyPair: currency_pair, rate: rate, amount: amount
  end

  def self.cancel_order( currency_pair, order_number, count )
    post 'cancelOrder', count, currencyPair: currency_pair, orderNumber: order_number
  end

  def self.cancel_margin_position( currency_pair, count )
    post 'closeMarginPosition', count, currencyPair: currency_pair
  end

  def self.withdraw( currency, amount, address, count )
    post 'withdraw', count, currency: currency, amount: amount, address: address
  end

  def self.return_chart_data( currency_pair, period, begin_time, end_time )
    get 'returnChartData', currencyPair: currency_pair, period: period, start: begin_time, end: end_time
  end

  protected

  def self.resource
    @@resouce ||= RestClient::Resource.new( 'https://www.poloniex.com' )
  end

  def self.get( command, params = {} )
    params[:command] = command
    resource[ 'public' ].get params: params
  end

  def self.post( command, count, params = {} )
    params[:command] = command
    params[:nonce]   = (Time.now.to_i * 1000) + count
    resource[ 'tradingApi' ].post params, { Key: configuration.key , Sign: create_sign( params ) }
  end

  def self.create_sign( data )
    encoded_data = Addressable::URI.form_encode( data )
    OpenSSL::HMAC.hexdigest( 'sha512', configuration.secret , encoded_data )
  end

end