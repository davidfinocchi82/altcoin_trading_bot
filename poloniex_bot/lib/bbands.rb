class Bb 
    $LOAD_PATH << '.'
    require 'lib/array.rb'

    def self.validate_data data, column, parameters
      # If this is a hash, choose which column of values to use for calculations.
      if data.is_a?(Hash)
        valid_data = data[column]
      else
        valid_data = data
      end

      if valid_data.length < parameters
        raise HelperException, "Data point length (#{valid_data.length}) must be greater than or equal to the required indicator periods (#{parameters})."
      end
      return valid_data
    end

    def self.calculate_sma data, parameters
      periods = parameters
      output = Array.new
      # Returns an array from the requested column and checks if there is enought data points.
      adj_closes = validate_data(data, :adj_close, periods)

      adj_closes.each_with_index do |adj_close, index|
        start = index+1-periods
        if index+1 >= periods
          adj_closes_sum = adj_closes[start..index].sum
          output[index] = (adj_closes_sum/periods.to_f)
        else
          output[index] = nil
        end
      end
      return output
      
    end

    # Middle Band = 20-day simple moving average (SMA)
    # Upper Band = 20-day SMA + (20-day standard deviation of price x 2) 
    # Lower Band = 20-day SMA - (20-day standard deviation of price x 2)
    def self.calculate data, parameters
      periods = parameters[0] 
      multiplier = parameters[1]
      output = Array.new
      adj_closes = validate_data(data, :adj_close, periods)

      adj_closes.each_with_index do |adj_close, index|
        start = index+1-periods
        if index+1 >= periods
          middle_band = calculate_sma(adj_closes[start..index], periods).last
          upper_band = middle_band + (adj_closes[start..index].standard_deviation * multiplier)
          lower_band = middle_band - (adj_closes[start..index].standard_deviation * multiplier)
          # Output for each point is [middle, upper, lower].
          output.push([middle_band, upper_band, lower_band])
        else
          output[index] = nil
        end
      end

      return output
      
    end

  end