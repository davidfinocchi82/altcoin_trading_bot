#millions of dollars 
#!/usr/bin/ruby

$LOAD_PATH << '.'
require 'byebug'
require 'lib/poloniex.rb'
require 'wamp_client'

pair_array = ["BTC_ETH", "BTC_XMR", "BTC_FCT", "BTC_LTC", "BTC_MAID", "BTC_XRP", "BTC_DASH", "BTC_STR", "BTC_DOGE", "BTC_BTS", "BTC_CLAM"]
puts "0: BTC_ETH\n1: BTC_XMR\n2: BTC_FCT\n3: BTC_LTC\n4: BTC_MAID\n5: BTC_XRP\n6: BTC_DASH\n7: BTC_STR\n8: BTC_DOGE\n9:BTC_BTS\n10: BTC_CLAM"
puts "Enter currency pair number 0-10: "
pair_index = Integer(gets) rescue nil

until pair_index.is_a?(Fixnum) do
  print "Please enter a number 0-10: "
  pair_index = Integer(gets) rescue nil
end
until pair_index < 10 do 
	puts "Enter a number 0-10: "
	pair_index = gets
end

@poloniex_pair = pair_array[pair_index]

options = {
    uri: 'wss://api.poloniex.com',
    realm: "realm1",
    verbose: true
}

connection = WampClient::Connection.new(options)
connection.on_join do |session, details|
	puts "Session Open"

	def handler(args, kwargs, details)
    	# TODO: Do Something!!
    	eth_price = []
		candles = []
		candles_close = []
		ema_10_arr = []
		ema_21_arr = []
		win_percent = 0.01

		decimal_threshold_1 = 8

		Poloniex.setup do | config |
		    config.key = 'O4NAD30O-X78Y7HRH-PXJVKFM5-OPS288RN'
		    config.secret = '9ba9310044f20ac612a2febc6fd20c299e410a8cf4a2491fade5dbd6ba38a47f44b3140b7419224f36e7efa33eabfb6be2f122646b402d760e39ab28817fdf0a'
		end

		def get_poloniex_balance(currency)
		    poloniex_balance_result = Poloniex.balances
		    result = JSON.parse(poloniex_balance_result.body)["exchange"][currency] rescue nil
		    if result == nil
		        return 0.0
		    else
		        return JSON.parse(poloniex_balance_result.body)["exchange"][currency]
		    end
		end

		def get_poloniex_bid(ticker, poloniex_pair)
		    return JSON.parse(ticker.body)[@poloniex_pair]["highestBid"]
		end

		def get_poloniex_ask(ticker, poloniex_pair)
		    return JSON.parse(ticker.body)[@poloniex_pair]["lowestAsk"]
		end

		def ema(closing, period, prev_ema)
			sum = closing.inject{ |sum,x| sum + x }
			sma = sum / period
			multi = (2.0 / (period + 1))
			return (closing[0] - prev_ema) * multi + prev_ema
		end

		eth_btc_history = Poloniex.get_trade_history(@poloniex_pair)
		current_rate = JSON.parse(eth_btc_history.body).first["rate"].to_f
		last_ema_10 = current_rate
		last_ema_21 = current_rate
		in_long_trade = false
		in_short_trade = false
		long_cross = false
		short_cross = false
		count = 0
		trade_price = 0.0
		losing_trades = 0.0
		winning_trades = 0.0

		File.delete("#{@poloniex_pair}_log.txt") if File.exist?("#{@poloniex_pair}_log.txt")
		File.new("#{@poloniex_pair}_log.txt", "w")
    	args.each do |trade|
			if trade[:type] == "newTrade"
				begin
					time = Time.now
					eth_btc_history = Poloniex.get_trade_history(@poloniex_pair)
					current_rate = trade[:data][:rate].to_f
				    puts time
				    puts "Rate: #{current_rate}"
				    puts "First: #{eth_price.first}"
				    if in_long_trade == true or in_short_trade == true 
				    	#(ema_10_arr[0] < ema_21_arr[0] and in_long_trade == true) or (ema_10_arr[0] > ema_21_arr[0] and in_short_trade == true)
				    	if current_rate < (trade_price - (trade_price * win_percent)) or current_rate > (trade_price + (trade_price * win_percent))
				    		#Poloniex.cancel_margin_position(@poloniex_pair)
				    		if in_short_trade == true and current_rate > (trade_price + (trade_price * win_percent))
				    			losing_trades = losing_trades + 1
				    			open("#{@poloniex_pair}_log.txt", 'a') { |f|
									f.puts "#{time} Losing short trade"
									if winning_trades > 0 and losing_trades > 0
										f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
									end
								}
							elsif in_long_trade == true and current_rate < (trade_price - (trade_price * win_percent))
								losing_trades = losing_trades + 1
								open("#{@poloniex_pair}_log.txt", 'a') { |f|
									f.puts "#{time} Losing long trade"
									if winning_trades > 0 and losing_trades > 0
										f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
									end
								}
							elsif in_short_trade == true and current_rate < (trade_price - (trade_price * win_percent))
								winning_trades = winning_trades + 1
								open("#{@poloniex_pair}_log.txt", 'a') { |f|
									f.puts "#{time} Winning short trade"
									if winning_trades > 0 and losing_trades > 0
										f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
									end
								}
							elsif in_long_trade == true and current_rate > (trade_price + (trade_price * win_percent))
								winning_trades = winning_trades + 1
								open("#{@poloniex_pair}_log.txt", 'a') { |f|
									f.puts "#{time} Winning long trade"
									if winning_trades > 0 and losing_trades > 0
										f.puts "Win percentage: #{(winning_trades / (winning_trades + losing_trades) * 100).round(2)}%\n"
									end
								}	
							end	
				    		short = false
							long = false
							close_above = false
							close_below = false
							in_long_trade = false
							in_short_trade = false
							long_cross = false
							short_cross = false
							count = 0
							trade_price = 0.0
				    	end
				    end
				    if time.min % 1 == 0 and in_long_trade == false and in_short_trade == false and time.sec == 0 #and time.min == 0 
				    	if candles.count >= 60
				    		candles.pop
				    		candles_close.pop
				    		ema_10_arr.pop
				    	end
				    	candles.unshift([eth_price.last, eth_price.first, eth_price.max, eth_price.min])
				    	candles_close.unshift(current_rate)
				    	ema_10 = ema(candles_close[0...9], 10.0, last_ema_10)
				    	ema_10_arr.unshift(ema_10)
				    	ema_21 = ema(candles_close[0...20], 21.0, last_ema_21)
				    	ema_21_arr.unshift(ema_21)
				    	puts "EMA 10: #{ema_10}"
				    	puts "EMA 21: #{ema_21}"
				    	if candles.count >= 22 
				    		if ema_10_arr[1] > ema_21_arr[1] and ema_10_arr[4] < ema_21_arr[4] 
				    			long_cross = true 
				    			short_cross = false
				    			count = 0
				    	# 		open("#{@poloniex_pair}_log.txt", 'a') { |f|
									# 	f.puts "#{time}, Long Cross!"
									# 	f.puts "Candles: #{candles[0]}"
									# 	f.puts "Candles close: #{candles_close[0]}"
									# 	f.puts "EMA 7: #{ema_10}\n"
									# }
				    		elsif ema_10_arr[1] < ema_21_arr[1] and ema_10_arr[4] > ema_21_arr[4]
				    			short_cross = true
				    			long_cross = false
				    			count = 0
				    	# 		open("#{@poloniex_pair}_log.txt", 'a') { |f|
									# 	f.puts "#{time}, Short Cross!"
									# 	f.puts "Candles: #{candles[0]}"
									# 	f.puts "Candles close: #{candles_close[0]}"
									# 	f.puts "EMA 7: #{ema_10}\n"
									# }
				    		end
				    		if long_cross == true and in_long_trade == false and in_short_trade == false
				    			#make long
				    			in_long_trade = true
				    			trade_price = current_rate
				    			open("#{@poloniex_pair}_log.txt", 'a') { |f|
									f.puts "#{time}, Making long trade"
									# f.puts "Candles: #{candles[0]}"
									# f.puts "Candles close: #{candles_close[0]}\n"
								}
				    		elsif short_cross == true and in_long_trade == false and in_short_trade == false
				    			#make short
				    			in_short_trade = true
				    			trade_price = current_rate
				    			open("#{@poloniex_pair}_log.txt", 'a') { |f|
									f.puts "#{time}, Making short trade"
									# f.puts "Candles: #{candles[0]}"
									# f.puts "Candles close: #{candles_close[0]}\n"
								}
				    		end		
					    	count = count + 1		
				    	end
				    	last_ema_10 = ema_10
				    	last_ema_21 = ema_21
					    eth_price = []
			    	end
			    rescue => e
			        puts "Error happended somewhere: #{e}"
			        retry
			    end
			    eth_price.unshift(current_rate)
	    	end
    	end
	end
	session.subscribe(@poloniex_pair, method(:handler))
end
connection.open
while true 
	
end